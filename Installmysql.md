# INSTALLATION ET UTILISATION DE MySQL

## INSTALLATION

 L'installation de ce paquet doit se faire de préférence en utilisant un terminal via la commande :

```sh
sudo apt install mysql-server
```

## DEMARAGE

Pour démarrer le serveur MySQL, tapez la commande suivante dans un terminal :

```sh
sudo systemctl start mysql
```

## REDEMARAGE

Pour redémarrer le serveur MySQL, tapez la commande suivante dans un terminal :

```sh
sudo systemctl restart mysql
```

## VERSION DE MySQL

Il est parfois utile de connaître la version installée:

```sh
mysql --version
```
## LANCER LA CONSOLE MySQL


 L’utilisateur root de MySQL est authentifié par son compte système (plugin auth_socket) et non plus par un mot de passe (plugin mysql_native_password).

```sh
sudo mysql
```

## LANCER UN FICHER SQL

pour lancer un fichier sql via un terminal:

```sh
sudo mysql 'nom_de_la_base_de_donnée' < nom_du_fichier.sql
```

la commande suivante se lance une fois que vous êtes dans MySQL à la suite de :

```sh
sudo mysql
```

```sql
SOURCE your_file.sql;
```
