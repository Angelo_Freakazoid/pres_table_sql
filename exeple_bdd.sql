-- Supprime la base de données 'boutique' si elle existe
DROP DATABASE IF EXISTS boutique;

-- Crée une nouvelle base de données appelée 'boutique'
CREATE DATABASE boutique;

-- Sélectionne la base de données 'boutique' pour être utilisée dans les opérations suivantes
USE boutique;

-- Créer un utilisateur
-- Vous avez le choix entre créer un utilisateur identifié au moyen d'un mot de passe 
-- (méthode mysql_native_password, celle par défaut pour MySQL) 
-- ou créer un accès MySQL à un utilisateur Ubuntu existant (méthode auth_socket). 
-- La plupart du temps, on utilisera mysql_native_password.
-- Pour créer un utilisateur MySQL identifié au moyen d'un mot de passe :
CREATE USER 'alucard'@'localhost' IDENTIFIED WITH mysql_native_password BY 'coucou123';

-- ou si cette commande échoue avec une erreur de syntaxe :
CREATE USER 'alucard'@'localhost' IDENTIFIED BY 'coucou123';

-- Accorde tous les privilèges sur toutes les tables de la base de données 'boutique' 
-- à l'utilisateur 'alucard' lorsqu'il se connecte depuis 'localhost'
GRANT ALL PRIVILEGES ON boutique.* TO 'alucard'@'localhost';

-- Supprimer les droits sur une base de données à un utilisateur précis
REVOKE ALL ON boutique.* FROM 'alucard'@'localhost';

-- Supprimer un utilisateur
-- Pour supprimer un utilisateur :
DROP USER 'alucard'@'localhost';

-- Crée une nouvelle table nommée 'livre'
CREATE TABLE livre (
    -- 'id' est une colonne de type entier. 
    -- AUTO_INCREMENT signifie que la valeur dans cette colonne s'incrémente automatiquement 
    -- pour chaque nouvelle ligne ajoutée.
    -- PRIMARY KEY indique que cette colonne est la clé primaire de la table, 
    -- c'est-à-dire un identifiant unique pour chaque enregistrement.
    id INT AUTO_INCREMENT PRIMARY KEY,

    -- 'titre' est une colonne de type VARCHAR, ce qui signifie "variable character".
    -- VARCHAR(50) signifie que cette colonne peut contenir des chaînes de caractères jusqu'à une longueur maximale 
    -- de 50 caractères.
    titre VARCHAR(50),

    -- 'nom_auteur' est maintenant une colonne de type VARCHAR, présumant que vous souhaitez stocker 
    -- le nom de l'auteur sous forme de texte.
    -- VARCHAR(50) permet de stocker des noms jusqu'à une longueur maximale de 50 caractères.
    nom_auteur VARCHAR(50),

    -- 'age_auteur' est une colonne de type entier.
    -- Cette colonne est destinée à stocker l'âge de l'auteur.
    age_auteur INT,

    -- 'année_parution' est une colonne de type DATE.
    -- Cette colonne est destinée à stocker la date de parution du livre.
    -- Le format DATE est généralement YYYY-MM-DD.
    année_parution DATE
);

-- Affiche une liste de toutes les bases de données présentes sur le serveur
SHOW DATABASES;

-- Affiche une liste de toutes les tables dans la base de données actuellement sélectionnée
SHOW TABLES;

-- Affiche la structure de la table 'livre', y compris les informations sur les colonnes
DESCRIBE livre;


-- Types de données numériques :
    INT ou INTEGER  -- Un entier standard.
    TINYINT  -- Un petit entier.
    SMALLINT  -- Un entier de petite taille.
    MEDIUMINT  -- Un entier de taille moyenne.
    BIGINT  -- Un très grand entier.
    FLOAT -- Un nombre à virgule flottante de précision simple.
    DOUBLE ou REAL  -- Un nombre à virgule flottante de précision double.
    DECIMAL ou NUMERIC -- Un type numérique à virgule fixe, utile pour stocker des valeurs monétaires par exemple.

-- Types de données de chaîne de caractères :
    VARCHAR -- Une chaîne de caractères de longueur variable.
    CHAR -- Une chaîne de caractères de longueur fixe.
    TEXT -- Une chaîne de caractères de grande taille.
    TINYTEXT -- Un texte de très petite taille.
    MEDIUMTEXT -- Un texte de taille moyenne.
    LONGTEXT -- Un texte de très grande taille.
    BLOB -- Un objet binaire de grande taille.
    TINYBLOB, MEDIUMBLOB, LONGBLOB -- Variantes de taille différente du type BLOB.

-- Types de données temporelles :
    DATE -- Une date.
    DATETIME -- Une combinaison de date et d'heure.
    TIMESTAMP -- Une combinaison de date et d'heure, utilisée pour enregistrer un instant précis.
    TIME -- Un moment de la journée.
    YEAR -- Une année.

-- Types de données spatiales :
    GEOMETRY -- Un type de base pour tous les types de données spatiales.
    POINT -- Une seule position dans l'espace.
    LINESTRING -- Une séquence de points formant une ligne.
    POLYGON -- Une forme géométrique à deux dimensions.


-- Autres types :
    ENUM -- Une liste de valeurs possibles prédéfinies.
    SET -- Un ensemble de zéro ou plusieurs valeurs choisies dans une liste de valeurs prédéfinies.